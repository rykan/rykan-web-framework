import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import external from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss'
import resolve from 'rollup-plugin-node-resolve'
import url from 'rollup-plugin-url'
import svgr from '@svgr/rollup'

import image from 'rollup-plugin-img';

import json from 'rollup-plugin-json';

import pkg from './package.json'

export default {
	input: 'src/index.js',
	output: [
		{
			file: pkg.main,
			format: 'cjs',
			sourcemap: true
		},
		{
			file: pkg.module,
			format: 'es',
			sourcemap: true
		}
	],
	plugins: [
		external(),
		postcss({
			modules: true
		}),
		url(),
		svgr(),
		babel({
			exclude: 'node_modules/**',
			plugins: [ 'external-helpers' ]
		}),
		resolve({
			extensions: ['.js', '.jsx', '.json'],
		}),
		image({
			output: `dist/images`
		}),
		json({
			// All JSON files will be parsed by default,
			// but you can also specifically include/exclude files
			//include: 'src/**',
			//exclude: 'node_modules/**',
			//exclude: ['node_modules/foo/**', 'node_modules/bar/**'],

			// for tree-shaking, properties will be declared as
			// variables, using either `var` or `const`
			preferConst: true, // Default: false

			// specify indentation for the generated default export —
			// defaults to '\t'
			// indent: '  ',

			// ignores indent and generates the smallest code
			compact: true, // Default: false

			// generate a named export for every property of the JSON object
			namedExports: false // Default: true
		}),
		commonjs()
	]
}
