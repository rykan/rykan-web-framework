import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './lists.css';

export default class List extends Component {
	static propTypes = {
		palceholder: PropTypes.string
	}

	render() {
		const {
			children,
			style,
		} = this.props

		return (
			<div className={`${styles.list} rwf-list`} style={style}>
				{children}
			</div>
		)
	}
}