import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './lists.css';

export default class ListItem extends Component {
	static propTypes = {
		children: PropTypes.string,
	}

	render() {
		const {
			children,
			disabled,
			onClick,
			style,
		} = this.props;

		return (
			<div className={disabled ? `${styles.listItem} ${styles.listItemDisabled}` : styles.listItem} onClick={onClick} style={style}>{children}</div>
		)
	}
}