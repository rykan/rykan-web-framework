import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

export default class extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			children,
			innerStyle,
			outerStyle,
		} = this.props;

		//console.log(outerStyle);

		return (
			<div className={styles.container} style={outerStyle}>
				<div className={styles["inner-container"]} style={innerStyle}>
					{children}
				</div>
			</div>
		)
	}
}