import React, { Component } from 'react';

import styles from './rwf.css';

export default class extends Component {
	render() {

		const {
			children,
			checked,
			onChange,
		} = this.props;

		return (
			<label className={styles.label}>
				<span>{children}</span>
				<input type="checkbox" class={styles["ios-switch"]} checked={checked} onChange={onChange} onInput={onChange} />
				<div className={styles.switch} />
			</label>
		)
	}
}