import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

export default class Footer extends Component {
	static propTypes = {
		children: PropTypes.any
	}

	render() {
		const {
			children,
			style,
		} = this.props

		return (
			<div className={styles.footer} style={style}>
				{children}
			</div>
		)
	}
}