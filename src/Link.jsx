import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './rwf.css'

export default class Link extends Component {
	static propTypes = {
		palceholder: PropTypes.string
	}

	render() {
		const {
			children,
			to
		} = this.props

		//console.log(this.context);
		//console.log(this);

		return (
			<a href={to} className={styles.link}>{children}</a>
		)
	}
}