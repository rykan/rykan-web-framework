import React, { Component } from 'react';

import styles from './rwf.css';

import Icon from '../icons/icon.js';

export default class extends Component {
	render() {

		const {
			//children,
			icon,
			onClick,
			margin,
			active,
			color,
			//w,
			//h,
		} = this.props;

		const size = this.props.size || 48;

		return (
			<div className={`${styles.metal} ${styles.radial} ${active ? styles.active : false}`} onClick={onClick} style={{ width: size, height: size, fontSize: size / 2, margin: margin, color: color, }} >
				<Icon type={icon}/>
			</div>
		)
	}
}