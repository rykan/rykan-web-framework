import React, { Component } from 'react'
import PropTypes from 'prop-types';

import Link from './Link.jsx';
import { Heading } from './headings/index.jsx';

import styles from './rwf.css'

export default class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: "",
			password: "",
		}
	}

	static propTypes = {
		palceholder: PropTypes.string
	}

	handleChange = event => {
		this.setState({
			[event.target.id]: event.target.value
		});
	}

	render() {
		var {
			placeholder,
			onSubmit,
			background,
		} = this.props

		background = background || "rgba(0, 0, 0, 0.6)";

		return (
			<form className={styles.login} onSubmit={onSubmit} style={{ background: background }}>
				<Heading>Sign In</Heading>
				<input type="email" placeholder={placeholder} className={styles.input} value={this.state.user} onChange={this.handleChange} id="user" />
				<input type="password" placeholder="Password" className={styles.input} value={this.state.password} onChange={this.handleChange} id="password" />
				<Link to="">Recover Your Account</Link>
				<button className={styles.confirm} type="submit">Sign In</button>
			</form>
		)
	}
}