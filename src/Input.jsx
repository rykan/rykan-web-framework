import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

export default class extends Component {
	static propTypes = {
		palceholder: PropTypes.string
	}

	render() {
		const {
			placeholder,
			type,
		} = this.props;

		return (
			<input type={type || "email"} placeholder={placeholder || "Email"} className={styles.input}/>
		)
	}
}