import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Import styles
import styles from './rwf-mini.css';
// Import internal components
import { Subtitle } from '../headings/index.jsx';

export default class Grid extends Component {
	static propTypes = {
		children: PropTypes.any,
		title: PropTypes.string,
	}

	render() {
		const {
			title,
			link,
			children,
		} = this.props;

		return (
			<div className={styles["grid-mini"]}>
				<div className={styles["grid-mini-title"]}><Subtitle>{title}</Subtitle></div>
				{children}
			</div>
		)
	}
}