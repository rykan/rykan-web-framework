import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Import styles
import styles from './rwf-mini.css';
// Import internal components
import { Base } from '../headings/index.jsx';
import Icon from '../icons/icon.js';

export default class Grid extends Component {
	static propTypes = {
		children: PropTypes.any,
		title: PropTypes.string,
	}

	render() {
		const {
			title,
			link,
			icon,
		} = this.props;

		return (
			<a className={styles["grid-item-mini"]} href={link}>
				<div className={styles["app-icon"]}><Icon type={icon}/></div>
				<Base>{title}</Base>
			</a>
		)
	}
}