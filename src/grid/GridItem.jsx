import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Import Styles
import styles from './rwf.css';

//import { Icon, Subheading, Text, Link } from '../index.js';
// Import internal components
import Icon from '../Icons/icon.js';
import { Subheading, Text } from '../headings/index.jsx';
import Link from '../Link.jsx';

export default class GridItem extends Component {
	static propTypes = {
		children: PropTypes.any,
		title: PropTypes.string,
	}

	render() {
		const {
			title,
			icon,
			text,
			link,
			src
		} = this.props

		//console.log(title);

		return (
			<div className={styles["grid-item"]}>
				{/*content[1] === "Axle" ? <StarfieldAnimation style={{ position: "absolute", width: "100%", height: "100%", top: "-1rem", left: "-2rem" }}/> : false*/}
				{icon !== "Axle" ? <Icon type={icon} /> : <img className={styles.icon} src={src} alt="Axle OS X Logo"/>}
				<Subheading>{title}</Subheading>
				<Text>{text}</Text>
				<Link to={link}>Learn More</Link>
			</div>
		)
	}
}