import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Import styles
import styles from './rwf.css';
// Import internal components
import { Subheading } from '../headings/index.jsx';
import Link from '../Link.jsx';

export default class Grid extends Component {
	static propTypes = {
		children: PropTypes.any,
		title: PropTypes.string,
	}

	render() {
		const {
			children,
			title,
			link,
		} = this.props

		return (
			<div className={styles["grid-content"]}>
				<div className={styles.title}><Subheading>{title}</Subheading>{link ? <Link to={link}>View All</Link> : false}</div>
				{children}
			</div>
		)
	}
}