import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf_video.css';

import ReactPlayer from 'react-player';

export default class Navbar extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			style,
		} = this.props

		return (
			<div className={styles.player} style={style}>
				<ReactPlayer>

				</ReactPlayer>
			</div>
		)
	}
}