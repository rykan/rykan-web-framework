import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

import { Heading } from './headings/index.jsx';

export default class Hero extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			title,
			style,
		} = this.props;

		return (
			<div className={styles.hero} style={style}>
				<Heading>{title}</Heading>
			</div>
		)
	}
}