import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Heading } from './headings/index.jsx';

import styles from './rwf.css';

export default class Signup extends Component {
	static propTypes = {
		palceholder: PropTypes.string
	}

	render() {
		const {
			placeholder,
		} = this.props

		return (
			<form className={styles.login}>
				<Heading>Sign Up</Heading>
				<div className={styles.names}>
					<input type="name" placeholder={"First Name"} className={styles.input}/>
					<input type="name" placeholder={"Last Name"} className={styles.input}/>
				</div>
				<input type="email" placeholder={"Email"} className={styles.input}/>
				<input type="password" placeholder="Password" className={styles.input}/>
				<input type="password" placeholder="Confirm Password" className={styles.input}/>
				<button className={styles.confirm} type="submit">Sign Up</button>
			</form>
		)
	}
}