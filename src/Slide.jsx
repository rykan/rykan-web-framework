import React, { Component } from 'react';
import PropTypes from 'prop-types';
// Import styles
import styles from './rwf.css';
// Import internal components
import { Heading, Subtitle } from './headings/Headings.jsx';
import Button from './Button.jsx';

export default class Slide extends Component {
	static propTypes = {
		children: PropTypes.any
	}

	render() {
		const {
			background,
			button,
			title,
			info,
			link,
			key,
		} = this.props

		return (
			<div className={styles["rykan-slide"]} key={key} style={{ background: `url(${background})` }}>
				<div className={styles["slide-wrapper"]}>
					<div className={styles["slide-content"]}>
						<div>
							<Heading>{title}</Heading>
							<Subtitle>{info}</Subtitle>
							<br/>
							<a href={link}>
								<Button>{button}</Button>
							</a>
						</div>
					</div>
				</div>
			</div>
		)
	}
}