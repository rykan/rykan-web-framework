import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

import { Tabs, Tab, TabList, TabPanel } from './components.js';
import Icon from '../icons/icon';

export default class extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			tabs,
			disabled,
			children,
			fileButton,
			style,
			tools,
			listStyle
		} = this.props;

		const tabsProps = tabs.map((tab) =>
			<Tab key={`${tab}-tab`} disabled={disabled ? true : false}>{tab}</Tab>
		)

		console.log(children);

		const panels = children.map((tab) =>
			<TabPanel key={`${tab}-panel`}>{tab}</TabPanel>
		)

		const toolbar = tools.map((tool) =>
			<Icon type={tool.type} onClick={tool.onClick}/>
		);

		return (
			<Tabs style={style}>
				{console.log("Context:")}
				{console.log(this.context)}

				<TabList style={listStyle} /* tabs={tabsProps} */>
					{fileButton}
					{/* <div className={styles.toolbar}>
						{toolbar}
					</div> */}
					{tabsProps}
				</TabList>

				{panels}
			</Tabs>
		)
	}
}