import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Dropdown from 'react-dropdown';

import Icon from '../icons/icon.js';

import styles from './rwf.css';

export default class extends Component {
	static defaultProps = {
		options: ["No Options specified.","Please check your source code"],
		palceholder: ""
	};

	static propTypes = {
		placeholder: PropTypes.string,
		options: PropTypes.array,
	};

	render() {
		const {
			options,
			placeholder,
			value,
		} = this.props;

		//const style = { gridColumn: `span ${size[0]}`, gridRow: `span ${size[1]}` };

		//const defaultValue = options[0] || value;

		return (
			<Dropdown
				options={options}
				value={value}
				placeholder={placeholder}
				className={styles["dropdown-root"]}
				controlClassName={styles["dropdown-control"]}
				placeholderClassName={styles["dropdown-placeholder"]}
				menuClassName={styles["dropdown-menu"]}
				arrowClassName={styles["dropdown-arrow"]}
				arrowClosed={<Icon type="ChevronDown"/>}
				arrowOpen={<Icon type="ChevronUp"/>}
			/>
		)
	}
}