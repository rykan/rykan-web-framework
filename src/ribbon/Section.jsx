import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

export default class extends Component {
	static defaultProps = {
		name: "please enter a name dummy!",
	};

	static propTypes = {
		children: PropTypes.any,
	};

	render() {
		const {
			children,
			name,
		} = this.props;

		return (
			<div className={styles["ribbon-section"]}>
				{children}
			</div>
		)
	}
}