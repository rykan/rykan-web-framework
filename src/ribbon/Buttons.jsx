import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Icon from '../icons/icon.js';

import styles from './rwf.css';

export default class extends Component {
	static defaultProps = {
		size: [1,1],
	};

	static propTypes = {
		children: PropTypes.any,
	};

	render() {
		const {
			size,
			icon,
			children
		} = this.props;

		//const className = `${styles.button} ${styles[`col-${size[0]}`]} ${styles[`row-${size[1]}`]}`

		const style = { gridColumn: `span ${size[0]}`, gridRow: `span ${size[1]}`, width: (size[0] * 12), height: (size[1] * 12) };

		// console.log(className);

		return (
			<div className={styles.button} style={style}>
				{children ? children : <Icon type={icon}/>}
			</div>
		)
	}
}