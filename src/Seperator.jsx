import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

export default class Signup extends Component {
	static propTypes = {
		palceholder: PropTypes.string
	}

	render() {
		const {
			direction,
		} = this.props

		return (
			direction === "row" ? <div className={styles.seperatorRow}/> : <div className={styles.seperatorCol}/>
		)
	}
}