import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from '../parallax.css';

import Parallax_Base from './base.jsx';
import Parallax_Back from './back.jsx';
import Parallax_Deep from './deep.jsx';
import Parallax_Fore from './fore.jsx';
import Parallax_Group from './group.jsx';
import Parallax_Layer from './layer.jsx';

class Parallax extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			children,
			debug,
			style,
		} = this.props

		return (
			<div className={`${styles.parallax}${debug ? styles[" debug-on"] : ""} rwf-parallax-root`} style={style}>
				{children}
			</div>
		)
	}
}

export {
	Parallax,
	Parallax_Base,
	Parallax_Back,
	Parallax_Deep,
	Parallax_Fore,
	Parallax_Group,
	Parallax_Layer,
}