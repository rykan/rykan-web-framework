import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from '../parallax.css';

export default class Parallax_Base extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			children,
		} = this.props

		return (
			<div className={`${styles.parallax__group}`}>
				{children}
			</div>
		)
	}
}