import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from '../parallax.css';

export default class Parallax_Layer extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			children,
			depth,
		} = this.props;

		var style = this.props.style || {};
		
		style.transform = "translateZ(0) scale(1)";

		depth != undefined ?
			depth < 0 ?
				style.transform = `translateZ(${depth * -90}px) scale(${1 - ((depth * -90) * -1) / -300})` :
				style.transform = `translateZ(${depth * -300}px) scale(${1 + ((depth * -300) * -1) / 300})` :
			false;

		return (
			<div className={`${styles.parallax__layer}`} style={style}>
				{children}
			</div>
		)
	}
}