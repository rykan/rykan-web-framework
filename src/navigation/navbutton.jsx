import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Base } from '../headings/Headings.jsx';

import styles from './rwf.css';

export default class Navbar extends Component {
	static propTypes = {
		children: PropTypes.string,
		link: PropTypes.string
	}

	render() {
		const {
			children,
			link,
			onClick,
			id,
			right
		} = this.props

		//console.log(`Align ${children} right? ${right}`);

		const floatRight = right ? { marginLeft: "auto" } : null;

		return (
			<div href={link} className={styles.navitem} id={id} style={floatRight} onClick={onClick}>
				<Base>{children}</Base>
			</div>
		)
	}
}