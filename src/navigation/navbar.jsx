import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

export default class Navbar extends Component {
	static propTypes = {
		children: PropTypes.any
	}

	render() {
		const {
			children,
			logo,
			style,
			outerStyle,
			fixed,
		} = this.props

		//var customStyles = style || {};

		fixed ? outerStyle.position = "fixed" : false;

		return (
			<div className={styles.navbar} style={outerStyle}>
				<div className={styles["navbar-inner"]} style={style}>
					<img className={styles.rykan} src={logo}/>
					{children}
				</div>
			</div>
		)
	}
}