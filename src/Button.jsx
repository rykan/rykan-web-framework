import React, { Component } from 'react'
import PropTypes from 'prop-types'

import styles from './rwf.css'

export default class Button extends Component {
	static propTypes = {
		children: PropTypes.any,
		disabled: PropTypes.bool,
	}

	render() {
		const {
			children,
			onClick,
			type,
			style
		} = this.props

		return (
			<button onClick={onClick} type={type} className={styles.button} style={style}>
				{children}
			</button>
		)
	}
}