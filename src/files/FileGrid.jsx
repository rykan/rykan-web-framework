import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

//import Icon from '../icons/icon.js';
//import { Base } from './headings/index.jsx';
import { Subtitle, Base, SubtitleAlt } from '../headings/index.jsx';
import Icon from '../icons/icon.js';

export default class Hero extends Component {
	static propTypes = {
		title: PropTypes.string,
	}

	render() {
		const {
			title,
			children,
		} = this.props;

		return (
			<div className={styles.grid}>
				<SubtitleAlt>{title}</SubtitleAlt>
				<div className={styles.file + " " + styles.header}>
					<div className={styles.icon}><Icon type="Document" /></div>
					<Base>Name</Base>
				</div>
				{children}
			</div>
		)
	}
}