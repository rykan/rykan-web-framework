import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './rwf.css';

import Icon from '../icons/icon.js';
import { Base } from '../headings/index.jsx';

export default class Hero extends Component {
	static propTypes = {
		children: PropTypes.any,
	}

	render() {
		const {
			name,
			format,
			app,
			id,
		} = this.props;

		return (
			<a className={styles.file} href={`/app/${app}/edit/${id}`}>
				<div className={styles.icon}><Icon type="Document" /></div>
				<Base>{name}.{format}</Base>
			</a>
		)
	}
}