import React, { Component } from 'react'

import { Theme, Button, Login, Signup, Link, Icon, Footer, List, ListItem, Seperator } from 'rykan-web-framework';
import { Heading, Subheading, Title, Subtitle, SubtitleAlt, Base, P, Text, Bold, I } from 'rykan-web-framework';

export default class extends Component {
	
	handleLogin = event => {
		event.preventDefault();

		window.alert("Signing in... (supposedly)");
	}

	render () {
		const buttonList = ["Button","Icon","Link","List","Login","Signup","Theme","Typography","Paragraph"];
		const buttons = buttonList.sort().map((button) => <a href={`/Components/${button}/`}><ListItem>{button}</ListItem></a> );
		return (
			<Theme background="http://512pixels.net/downloads/macos-wallpapers/10-3.png">
				<div className="container">
					<List>
						{buttons}
					</List>
					<div className="subcontainer">
						<div>
							<Button onClick={() => window.alert("You Clicked Me! Yay!")}>Click Me!</Button>
							<Button>Click Me Too!!!! (I don't do anything though....)</Button>
							<Text>This is a page of demos for components in the Rykan Web Framework,</Text>
							<Bold>which has features such as a bold font component <I>(works with italic too!)</I></Bold>
							<Text>and more interesting things like login boxes.</Text>
							<Heading>Login Examples</Heading>
							<Subheading>E-mail</Subheading>
							<Login placeholder="Email" onSubmit={this.handleLogin}/>
							<Link to="">View full demo</Link>
							<Heading>Sign-up Example</Heading>
							<Signup/>
							<Link to="">View full demo</Link>
							<P>A large block of text such as this. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sem orci, laoreet sit amet odio nec, commodo volutpat tellus. Nullam at nisl mollis, auctor dolor eu, tempus diam. Ut non accumsan augue. Aenean non tellus libero. Nam condimentum lorem a volutpat fermentum. Duis sed feugiat sapien, in pharetra ex. Ut elementum efficitur justo, at pulvinar dolor blandit eget. Maecenas ornare commodo augue. </P>
							<Heading>Seperator and List</Heading>
							<List>
								<ListItem>Example 1</ListItem>
								<ListItem>Example 2</ListItem>
								<ListItem><Seperator/></ListItem>
								<ListItem>Example 3</ListItem>
								<ListItem>Example 4</ListItem>
							</List>
						</div>
					</div>
				</div>
				<Footer/>
			</Theme>
		)
	}
}
