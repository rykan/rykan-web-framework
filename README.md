# rykan-web-framework

> Rykan Design Language for ReactJS

[![NPM](https://img.shields.io/npm/v/rykan-web-framework.svg)](https://www.npmjs.com/package/rykan-web-framework) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save rykan-web-framework
```

```bash
yarn add rykan-web-framework
```

## Usage

### Simple theme and Heading

```jsx
import React, { Component } from 'react'

import {
    Theme,
    makeTheme,
} from 'rykan-web-framework'

const theme = makeTheme({
    theme: "dark", // "dark" or "light"
    accent: "#0078D7" // can be any six-digit hex
})

class Example extends Component {
    render() {
        return(
            <Theme theme={theme}>
                <Heading>RWF App!</Heading>
            </Theme>
        )
    }
}
```

# Docs

<!-- [API Reference](https://docs.rykan.tech/web-framework) -->

[API Reference](https://gitlab.com/rykan/rykan-web-framework/wiki/)

[Demos](https://rykan.gitlab.io/rwf-demos/)

[Examples](https://git.rykan.tech/rwf-examples/)

# License

MIT © [](https://github.com/)
